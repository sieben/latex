Grille de lecture de l'entretien pour le recruteur
==================================================

- Evaluation de la maturité d'un candidat (rationnel, concret, homme de
  terrain)
- Équilibre quotient intellectuel et quotient émotionnel (connaissance
  de soi et des autres)
- Capable de créer un environnement interactif
- Apprécie la relation humaine / Adaptation du discours selon les
  milieux socioculturels
- En Équipe ; obtient des résultats après évaluation des potentiels
- Fait face à ses responsabilités
- Recherche toujours des occasions de développement
- Cherche à apprendre sans cesse / A approfondir ses compétences, son
  métier
- Adaptation rapide à d'autres cultures
- Tourné vers l'action
- Sait marquer sa différence / Sait défendre ses positions
- Vision "helico" (analyse, synthèse rapide de données et prise de
  décision)
- Souci d'amélioration sans cesse ses résultats
- Capable de positiver à partir d'un echec / Utiliser les critiques
  négatives de ses collègues
- La manière d'obtenir le résultat importe davantage que le résultat
  lui-même

Choses à savoir
===============

- Sa taille, son identité, son activité, ses produits, le nom de son PDG
- Est ce qu'elle appartient à un groupe, si elle est implantée en France
- Secteur, marché, clients, concurrence
- Ses performances, leader ou non, chiffre de croissance, ses points
  forts
- Methodes de travail, techniques d'intégration particulières,
  actualité
- Ouvertures
- Projet du moment
